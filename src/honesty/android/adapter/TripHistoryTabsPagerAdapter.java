package honesty.android.adapter;



import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import honesty.android.fragments.ApplicationFragment;
import honesty.android.fragments.ServiceFragment;
import honesty.android.fragments.SettingsFragment;

import honesty.android.fragments.ApplicationFragment;
import honesty.android.fragments.ServiceFragment;


public class TripHistoryTabsPagerAdapter extends FragmentPagerAdapter {

    FragmentManager fm;

    public TripHistoryTabsPagerAdapter(FragmentManager fm) {
        super(fm);
        this.fm=fm;

    }

    @Override
    public Fragment getItem(int position) {



        switch (position){

            case 0:
                ApplicationFragment applicationFragment =new ApplicationFragment();
                return applicationFragment;

            case 1:
                ServiceFragment applicationFragment1 =new ServiceFragment();
                return applicationFragment1;

            case 2:
                SettingsFragment applicationFragment2 =new SettingsFragment();
                return applicationFragment2;


        }
        return new ApplicationFragment();
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        switch (position){
            case 0:
                return "Apps";
            case 1:
                return "Services";
            case 2:
                return "Settings";

        }
        return "Apps";
    }
}