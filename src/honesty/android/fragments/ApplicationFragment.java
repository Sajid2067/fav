package honesty.android.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import honesty.android.Settings.ButtonChangeActivity;
import honesty.android.favourite.CallActivity;
import honesty.android.favourite.Item;
import honesty.android.favourite.MessageActivity;
import honesty.android.favourite.R;
import honesty.android.favourite.database.DBAdapterAppList;
import honesty.android.section.model.AppAdapter;
import honesty.android.section.model.Description;
import honesty.android.section.model.SectionItem;
import honesty.android.section.model.ServiceList;
import honesty.android.section.model.TabChangedCount;


public class ApplicationFragment extends Fragment {

    public static ArrayList<Item> items;
    private PackageManager packageManager = null;
    public static List<ApplicationInfo> applist = null;
    public static List<ApplicationInfo> applistListed = null;
    DBAdapterAppList db;
    public static AppAdapter adapter;
    ListView l1, l2;
    boolean isUninstall = true;
    protected View rootView;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    SharedPreferences.Editor editor;
    public InterstitialAd mInterstitialAd;
    // public static int p=0;
    @SuppressWarnings("deprecation")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    private List<ApplicationInfo> checkForLaunchIntent(List<ApplicationInfo> list) {
        ArrayList<ApplicationInfo> applist = new ArrayList<ApplicationInfo>();

        for (ApplicationInfo info : list) {
            try {
                if (null != packageManager.getLaunchIntentForPackage(info.packageName)) {
                    applist.add(info);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        return applist;
    }

    private class LoadApplications extends AsyncTask<Void, Void, Void> {
        private ProgressDialog progress = null;

        @Override
        protected Void doInBackground(Void... params) {
            applist = checkForLaunchIntent(packageManager.getInstalledApplications(PackageManager.GET_META_DATA));
            Collections.sort(applist, new ApplicationInfo.DisplayNameComparator(packageManager));

            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected void onPostExecute(Void result) {
           applistListed.clear();
            items.clear();

            db.open();

            try {
                Cursor c = db.getAllAppList();
                if (c != null & c.moveToFirst()) {
                    do {
                        String name = c.getString(2);
                        isUninstall = true;
                        for (int j = 0; j < applist.size(); j++) {
                            if (applist.get(j).packageName.compareTo(c.getString(2)) == 0) {

                                applistListed.add(applist.get(j));
                                applist.remove(j);
                                isUninstall = false;
                            }

                        }
                        if (isUninstall == true) {
                            db.deleteAppList(name);
                        }

                    } while (c.moveToNext());
                }
            } catch (Exception e) {
                System.out.println(e);
            }
            db.close();


            if (applistListed.size() == 0) {
                applistListed.add(applist.get(0));
                applistListed.add(applist.get(1));

                db.open();
                String tempName = applist.get(0).loadLabel(packageManager).toString();

                db.insertAppList(applist.get(0).packageName, tempName);
                db.insertAppList(applist.get(1).packageName, applist.get(1).loadLabel(packageManager).toString());

                db.close();

                applist.remove(0);
                applist.remove(0);
            }


            items.add(new SectionItem("Added"));
            for (int i = 0; i < applistListed.size(); i++) {
                items.add(new Description(applistListed.get(i), true));
            }


            items.add(new SectionItem("Not Added"));
            for (int i = 0; i < applist.size(); i++) {
                items.add(new Description(applist.get(i), false));

            }


            adapter = new AppAdapter(getActivity(), items, l1);

            adapter.notifyDataSetChanged();

            l1.setAdapter(adapter);


            progress.dismiss();
            super.onPostExecute(result);
        }

        @Override
        protected void onPreExecute() {
            progress = ProgressDialog.show(getActivity(), null,
                    "Loading application info...");
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(honesty.android.favourite.R.layout.general, container, false);
        }
        l1 = (ListView) rootView.findViewById(honesty.android.favourite.R.id.list1);

        AdView mAdView = new AdView(getActivity());
        mAdView.setAdUnitId(getResources().getString(R.string.banner_home_footer));
        mAdView.setAdSize(AdSize.BANNER);

        LinearLayout layout = (LinearLayout) rootView.findViewById(R.id.footerLayout);
        layout.addView(mAdView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        TabChangedCount tabChangedCount=new TabChangedCount(getActivity());
        long tabChanged = tabChangedCount.getTabChanged();
        if (tabChanged % 4 == 0 && tabChanged >4) {
            //Showing Add

            mInterstitialAd = new InterstitialAd(getActivity());

            // set the ad unit ID
            mInterstitialAd.setAdUnitId(getActivity().getString(R.string.interstitial_full_screen));
            // Load ads into Interstitial Ads
            mInterstitialAd.loadAd(adRequest);
            mInterstitialAd.setAdListener(new AdListener() {
                public void onAdLoaded() {
                    showInterstitial();
                }
            });
            Log.d("Adv","Request for adv");

        }
        tabChanged++;
        tabChangedCount.setTabChanged(tabChanged);
        Log.d("tabChanged", tabChanged+"");

        db = new DBAdapterAppList(getActivity());
        if (applist == null) {
            applist = new ArrayList<ApplicationInfo>();
        }
        if (applistListed == null) {
            applistListed = new ArrayList<ApplicationInfo>();
        }
        if (items == null) {
            items = new ArrayList<Item>();
        }

        //initialization of beginning
        if (ServiceFragment.servicelist == null) {


            if (ServiceFragment.servicelist == null) {
                ServiceFragment.servicelist = new ArrayList<ServiceList>();
                ServiceFragment.servicelist.add(new ServiceList("WI-FI", "off"));
                ServiceFragment.servicelist.add(new ServiceList("BLUETOOTH", "off"));
                ServiceFragment.servicelist.add(new ServiceList("ROTATION", "off"));
                // servicelist.add(new ServiceList("DATA","off"));
                ServiceFragment.servicelist.add(new ServiceList("HOTSPOT", "off"));
                ServiceFragment.servicelist.add(new ServiceList("GPS", "off"));
                ServiceFragment.servicelist.add(new ServiceList("SOUND", "off"));
                // 	 servicelist.add(new ServiceList("LOCATION","off"));
                ServiceFragment.servicelist.add(new ServiceList("NFC", "off"));
            }
            if (ServiceFragment.servicelistListed == null) {
                ServiceFragment.servicelistListed = new ArrayList<ServiceList>();
            }
            if (ServiceFragment.items == null) {
                ServiceFragment.items = new ArrayList<Item>();
            }


            db = new DBAdapterAppList(getActivity());
            db.open();

            try {
                Cursor c = db.getAllServiceList();
                if (c != null & c.moveToFirst()) {
                    do {
                        for (int j = 0; j < ServiceFragment.servicelist.size(); j++) {
                            if (ServiceFragment.servicelist.get(j).getName().compareTo(c.getString(1)) == 0) {
                                ServiceFragment.servicelistListed.add(ServiceFragment.servicelist.get(j));
                                ServiceFragment.servicelist.remove(j);

                            }

                        }

                    } while (c.moveToNext());
                }
            } catch (Exception e) {
                System.out.println(e);
            }
            db.close();

            if (ServiceFragment.servicelistListed.size() == 0) {
                ServiceFragment.servicelistListed.add(new ServiceList(ServiceFragment.servicelist.get(0).getName(), "on"));
                ServiceFragment.servicelistListed.add(new ServiceList(ServiceFragment.servicelist.get(1).getName(), "on"));

                db.open();

                db.insertServiceList(ServiceFragment.servicelist.get(0).getName());
                db.insertServiceList(ServiceFragment.servicelist.get(1).getName());

                db.close();

                ServiceFragment.servicelist.remove(0);
                ServiceFragment.servicelist.remove(0);
            }

        }

        // applistNotListed = new ArrayList<ApplicationInfo>();

        packageManager = getActivity().getPackageManager();

        new LoadApplications().execute();

        return rootView;


    }
    private void showInterstitial() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }

    /**
     * Event Handling for Individual menu item selected
     * Identify single menu item by it's id
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case honesty.android.favourite.R.id.menu_Call:
                // Single menu item is selected do something
                // Ex: launching new activity/screen or show alert message
                Intent i = new Intent(getActivity(), CallActivity.class);
                startActivity(i);
                return true;

            case honesty.android.favourite.R.id.menu_Message:
                Intent in = new Intent(getActivity(), MessageActivity.class);
                startActivity(in);

                return true;

            //   case honesty.android.favourite.R.id.menu_Boot:
            // Intent inte=new Intent(this,BootActivity.class);
            //	startActivity(inte);
            //
            //    return true;

            case honesty.android.favourite.R.id.menu_Icon:
                Intent intent = new Intent(getActivity(), ButtonChangeActivity.class);
                startActivity(intent);

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


}