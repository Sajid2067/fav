package honesty.android.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import java.util.ArrayList;
import java.util.List;

import honesty.android.Settings.ButtonChangeActivity;
import honesty.android.favourite.CallActivity;
import honesty.android.favourite.Item;
import honesty.android.favourite.MessageActivity;
import honesty.android.favourite.R;
import honesty.android.favourite.database.DBAdapterAppList;
import honesty.android.section.model.SectionItem;
import honesty.android.section.model.ServiceAdapter;
import honesty.android.section.model.ServiceList;
import honesty.android.section.model.TabChangedCount;


public class ServiceFragment extends Fragment {

    public static ArrayList<Item> items;
    private PackageManager packageManager = null;
    public static List<ServiceList> servicelist = null;
    public static List<ServiceList> servicelistListed = null;
    DBAdapterAppList db;
    public static ServiceAdapter adapter;
    ListView l1, l2;
    boolean isUninstall = true;
    protected View rootView;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    SharedPreferences.Editor editor;
    public InterstitialAd mInterstitialAd;

    // public static int p=0;
    @SuppressWarnings("deprecation")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    private class LoadApplications extends AsyncTask<Void, Void, Void> {
        private ProgressDialog progress = null;

        @Override
        protected Void doInBackground(Void... params) {

            servicelist.clear();

            servicelist.add(new ServiceList("WI-FI", "off"));
            servicelist.add(new ServiceList("BLUETOOTH", "off"));
            servicelist.add(new ServiceList("ROTATION", "off"));
            // servicelist.add(new ServiceList("DATA","off"));
            servicelist.add(new ServiceList("HOTSPOT", "off"));
            servicelist.add(new ServiceList("GPS", "off"));
            servicelist.add(new ServiceList("SOUND", "off"));
            // 	 servicelist.add(new ServiceList("LOCATION","off"));
            servicelist.add(new ServiceList("NFC", "off"));
            //servicelist.add(new ServiceList("POWER SAVING","off"));


            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected void onPostExecute(Void result) {
            servicelistListed.clear();
            items.clear();
            db.open();

            try {
                Cursor c = db.getAllServiceList();
                if (c != null & c.moveToFirst()) {
                    do {
                        for (int j = 0; j < servicelist.size(); j++) {
                            String name = c.getString(1);
                            if (servicelist.get(j).getName().compareTo(c.getString(1)) == 0) {
                                servicelistListed.add(servicelist.get(j));
                                servicelist.remove(j);

                            }

                        }

                    } while (c.moveToNext());
                }
            } catch (Exception e) {
                System.out.println(e);
            }
            db.close();

            if (servicelistListed.size() == 0) {
                servicelistListed.add(new ServiceList(servicelist.get(0).getName(), "on"));
                servicelistListed.add(new ServiceList(servicelist.get(1).getName(), "on"));

                db.open();

                db.insertServiceList(servicelist.get(0).getName());
                db.insertServiceList(servicelist.get(1).getName());

                db.close();

                servicelist.remove(0);
                servicelist.remove(0);
            }

            items.add(new SectionItem("Added"));
            for (int i = 0; i < servicelistListed.size(); i++) {
                items.add(new ServiceList(servicelistListed.get(i).getName(), "on"));
            }


            items.add(new SectionItem("Not Added"));
            for (int i = 0; i < servicelist.size(); i++) {
                items.add(new ServiceList(servicelist.get(i).getName(), "off"));
            }


            adapter = new ServiceAdapter(getActivity(), items, l1);

            adapter.notifyDataSetChanged();

            l1.setAdapter(adapter);

            progress.dismiss();
            super.onPostExecute(result);
        }

        @Override
        protected void onPreExecute() {
            progress = ProgressDialog.show(getActivity(), null,
                    "Loading Service info...");
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.general, container, false);
        }


        l1 = (ListView) rootView.findViewById(honesty.android.favourite.R.id.list1);
        AdView mAdView = new AdView(getActivity());
        mAdView.setAdUnitId(getResources().getString(R.string.banner_home_footer));
        mAdView.setAdSize(AdSize.BANNER);

        LinearLayout layout = (LinearLayout) rootView.findViewById(R.id.footerLayout);
        layout.addView(mAdView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        TabChangedCount tabChangedCount=new TabChangedCount(getActivity());
        long tabChanged = tabChangedCount.getTabChanged();
        if (tabChanged % 4 == 0 && tabChanged >4) {
            //Showing Add

            mInterstitialAd = new InterstitialAd(getActivity());

            // set the ad unit ID
            mInterstitialAd.setAdUnitId(getActivity().getString(R.string.interstitial_full_screen));
            // Load ads into Interstitial Ads
            mInterstitialAd.loadAd(adRequest);
            mInterstitialAd.setAdListener(new AdListener() {
                public void onAdLoaded() {
                    showInterstitial();
                }
            });
            Log.d("Adv", "Request for adv");

        }
        tabChanged++;
        tabChangedCount.setTabChanged(tabChanged);
        Log.d("tabChanged", tabChanged+"");

        db = new DBAdapterAppList(getActivity());

        if (servicelist == null) {
            servicelist = new ArrayList<ServiceList>();
        }
        if (servicelistListed == null) {
            servicelistListed = new ArrayList<ServiceList>();
        }

        if (items == null) {
            items = new ArrayList<Item>();
        }

        packageManager = getActivity().getPackageManager();

        new LoadApplications().execute();

        return rootView;


    }

    /**
     * Event Handling for Individual menu item selected
     * Identify single menu item by it's id
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case honesty.android.favourite.R.id.menu_Call:
                // Single menu item is selected do something
                // Ex: launching new activity/screen or show alert message
                Intent i = new Intent(getActivity(), CallActivity.class);
                startActivity(i);
                return true;

            case honesty.android.favourite.R.id.menu_Message:
                Intent in = new Intent(getActivity(), MessageActivity.class);
                startActivity(in);

                return true;

            //   case honesty.android.favourite.R.id.menu_Boot:
            // Intent inte=new Intent(this,BootActivity.class);
            //	startActivity(inte);
            //
            //     return true;

            case honesty.android.favourite.R.id.menu_Icon:
                Intent intent = new Intent(getActivity(), ButtonChangeActivity.class);
                startActivity(intent);

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showInterstitial() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }


}