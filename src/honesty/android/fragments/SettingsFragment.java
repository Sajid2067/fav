package honesty.android.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import honesty.android.Settings.ButtonChangeActivity;
import honesty.android.Settings.ButtonSizeActivity;
import honesty.android.Settings.HelpActivity;
import honesty.android.Settings.SettingsAdapter;
import honesty.android.Settings.SettingsItem;
import honesty.android.favourite.CallActivity;
import honesty.android.favourite.HUD;
import honesty.android.favourite.Item;
import honesty.android.favourite.MessageActivity;
import honesty.android.favourite.R;
import honesty.android.favourite.database.DBAdapterAppList;

import java.util.ArrayList;
import java.util.List;

import honesty.android.section.model.TabChangedCount;


public class SettingsFragment extends Fragment {

	public static ArrayList<Item> items ;
	private PackageManager packageManager = null;
	public static List<ApplicationInfo> applist = null;
	public  static List<ApplicationInfo> applistListed = null;
	DBAdapterAppList db;
	public static SettingsAdapter adapter;
	SharedPreferences sharedpreferences;
	public static final String MyPREFERENCES = "MyPrefs" ;
	SharedPreferences.Editor editor;
	ListView l1,l2;
	boolean flag=true;
	View rootView;
	public InterstitialAd mInterstitialAd;
	   // public static int p=0;
	 @SuppressWarnings("deprecation")
		@Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);


	    }


	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.general, container, false);
		}

		l1=(ListView)rootView.findViewById(honesty.android.favourite.R.id.list1);
		AdView mAdView = new AdView(getActivity());
		mAdView.setAdUnitId(getResources().getString(R.string.banner_home_footer));
		mAdView.setAdSize(AdSize.BANNER);



		LinearLayout layout = (LinearLayout)rootView.findViewById(R.id.footerLayout);
		layout.addView(mAdView);
		AdRequest adRequest = new AdRequest.Builder().build();
		mAdView.loadAd(adRequest);
		items = new ArrayList<Item>();

		sharedpreferences= PreferenceManager.getDefaultSharedPreferences(getActivity());
		editor = sharedpreferences.edit();

		TabChangedCount tabChangedCount=new TabChangedCount(getActivity());
		long tabChanged = tabChangedCount.getTabChanged();
		if (tabChanged % 4 == 0 && tabChanged >4) {
			//Showing Add

			mInterstitialAd = new InterstitialAd(getActivity());

			// set the ad unit ID
			mInterstitialAd.setAdUnitId(getActivity().getString(R.string.interstitial_full_screen));
			// Load ads into Interstitial Ads
			mInterstitialAd.loadAd(adRequest);
			mInterstitialAd.setAdListener(new AdListener() {
				public void onAdLoaded() {
					showInterstitial();
				}
			});
			Log.d("Adv", "Request for adv");

		}
		tabChanged++;
		tabChangedCount.setTabChanged(tabChanged);
		Log.d("tabChanged", tabChanged+"");

		String	boot=sharedpreferences.getString("bootStatus", "1");
//		if(boot.compareTo("0")==0){
//			flag=false;
//		}else{
//			flag=true;
//		}
//		items.add(new SettingsItem("Start on re-start",flag));
		items.add(new SettingsItem("Button Change",true));
		items.add(new SettingsItem("Button size",true));
		items.add(new SettingsItem("Call Favourite",true));
		items.add(new SettingsItem("Message Favourite",true));
		items.add(new SettingsItem("Help",true));
		//items.add(new SettingsItem("About",true));
		items.add(new SettingsItem("Exit",true));

		adapter = new SettingsAdapter(getActivity(), items ,l1);

		//	adapter.notifyDataSetChanged();

		l1.setAdapter(adapter);

		l1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
									int position, long id) {
				// When clicked, show a toast with the TextView text
				SettingsItem setItem = (SettingsItem) parent.getItemAtPosition(position);

				String itemName=setItem.getName();

				if(itemName.compareTo("Button Change")==0){
					Intent i=new Intent(getActivity(),ButtonChangeActivity.class);
					startActivity(i);
				}else if(itemName.compareTo("Button size")==0){
					Intent i=new Intent(getActivity(),ButtonSizeActivity.class);
					startActivity(i);
				}else if(itemName.compareTo("Call Favourite")==0){
					Intent i=new Intent(getActivity(),CallActivity.class);
					startActivity(i);
				}else if(itemName.compareTo("Message Favourite")==0){
					Intent in=new Intent(getActivity(),MessageActivity.class);
					startActivity(in);
				}else if(itemName.compareTo("Exit")==0){
					LayoutInflater inflater = getActivity().getLayoutInflater();
					final View dialogview = inflater.inflate(honesty.android.favourite.R.layout.alert_dialouge_exit, null);

					AlertDialog alertDialogCategory = new AlertDialog.Builder(
							getActivity())
							.setView(dialogview)
							.setPositiveButton("Yes",
									new DialogInterface.OnClickListener() {

										@Override
										public void onClick(DialogInterface arg0,
															int arg1) {
											// TODO Auto-generated method stub

											Intent intent = new Intent(getActivity(), HUD.class);
											getActivity().stopService(intent);
											getActivity().finish();
										}

									})
							.setNegativeButton("Cancel",
									new DialogInterface.OnClickListener() {

										@Override
										public void onClick(DialogInterface dialog,
															int which) {
											// TODO Auto-generated method stub

										}

									}).create();

					alertDialogCategory.show();

				}else if(itemName.compareTo("Help")==0){
					Intent intent=new Intent(getActivity(),HelpActivity.class);
					startActivity(intent);
//						    }else if(itemName.compareTo("About")==0){
//						    	Intent intent=new Intent(getActivity(),AboutActivity.class);
//					        	startActivity(intent);
				}


			}
		});




		return rootView;


	}

	private void showInterstitial() {
		if (mInterstitialAd.isLoaded()) {
			mInterstitialAd.show();
		}
	}

	/**
	     * Event Handling for Individual menu item selected
	     * Identify single menu item by it's id
	     * */
	    @Override
	    public boolean onOptionsItemSelected(MenuItem item)
	    {
	         
	        switch (item.getItemId())
	        {
	        case honesty.android.favourite.R.id.menu_Call:
	            // Single menu item is selected do something
	            // Ex: launching new activity/screen or show alert message
	          Intent i=new Intent(getActivity(),CallActivity.class);
	        	startActivity(i);
	            return true;
	 
	        case honesty.android.favourite.R.id.menu_Message:
	        	 Intent in=new Intent(getActivity(),MessageActivity.class);
	         	startActivity(in);
	        	
	            return true;
	            
	       // case honesty.android.favourite.R.id.menu_Boot:
	       	// Intent inte=new Intent(this,BootActivity.class);
	        //	startActivity(inte);
	       	//
	        //   return true;
	           
	        case honesty.android.favourite.R.id.menu_Icon:
	       	 Intent intent=new Intent(getActivity(),ButtonChangeActivity.class);
	        	startActivity(intent);
	       	
	           return true;

	        default:
	            return super.onOptionsItemSelected(item);
	        }
	    }    
	    
	    
	}