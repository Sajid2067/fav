package honesty.android.section.model;

import honesty.android.favourite.Item;


public class ServiceList implements Item{

	
	String appinfo;
	String selected = "off";

	public ServiceList(String appinfo,String selected) {
		
		this.appinfo = appinfo;
		this.selected=selected;
	}
	
	@Override
	public boolean isSection() {
		return false;
	}

	@Override
	public boolean isDescription() {
		return true;
	}
	
	 public String isSelected() {  
		 return selected;  
		 } 
	 
	 public void setSelected(String selected) { 
		 this.selected = selected;  
		 } 
	 
	 public String getName() {   return appinfo;  } 
	 public String getState() {   return selected;  } 

}
