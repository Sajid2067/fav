package honesty.android.section.model;

import android.content.pm.ApplicationInfo;

import honesty.android.favourite.Item;


public class Description implements Item {

	
	public ApplicationInfo appinfo;
	boolean selected = false;

	public Description(ApplicationInfo appinfo,boolean selected) {
		
		this.appinfo = appinfo;
		this.selected=selected;
	}
	
	@Override
	public boolean isSection() {
		return false;
	}

	@Override
	public boolean isDescription() {
		return true;
	}
	
	 public boolean isSelected() {  
		 return selected;  
		 } 
	 
	 public void setSelected(boolean selected) { 
		 this.selected = selected;  
		 } 
	 
	 public ApplicationInfo getName() {   return appinfo;  } 

}
