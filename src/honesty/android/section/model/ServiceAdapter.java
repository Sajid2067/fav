package honesty.android.section.model;

import java.util.ArrayList;

import honesty.android.fragments.ServiceFragment;

import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import honesty.android.favourite.Item;
import honesty.android.favourite.database.DBAdapterAppList;

public class ServiceAdapter extends ArrayAdapter<Item> {

	private Context context;
	private ArrayList<Item> items;
	private LayoutInflater vi;
	final PackageManager packageManager;
	DBAdapterAppList db;
	ServiceList ei;
	Contacts co;
	CheckBox selection;
	View keep;
	int j=0;
	int temp=0;	
	
	public ServiceAdapter(Context context, ArrayList<Item> items, ListView lst) {
		super(context, 0, items);
		this.context = context;
		this.items = items;
		
		packageManager = context.getPackageManager();
		vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		db = new DBAdapterAppList(context);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;

		WHolder wholder;
		
		final Item i = items.get(position);

		
			
			  wholder = new WHolder();
			  v = vi.inflate(honesty.android.favourite.R.layout.wether_list_row, null);
				wholder.wname = (TextView) v.findViewById(honesty.android.favourite.R.id.app_name);
		        wholder.wname.setTextColor(Color.WHITE);
		        wholder.wname.setTextSize(20);
				//wholder.wpackag = (TextView) v.findViewById(R.id.app_paackage);
				wholder.wimage = (ImageView) v.findViewById(honesty.android.favourite.R.id.app_icon);
				wholder.wselection = (CheckBox) v
						.findViewById(honesty.android.favourite.R.id.checkBoxSelection);
				
				v.setTag(wholder);
				
		
				
			if (i.isSection() ) {
				
				
				SectionItem si = (SectionItem) i;
				wholder.wname.setText(si.getTitle());
				wholder.wname.setTextSize(20);
				wholder.wname.setGravity(Gravity.CENTER_HORIZONTAL);
				wholder.wname.setTextColor(Color.WHITE);
				
				v.setOnClickListener(null);
				v.setOnLongClickListener(null);
				v.setLongClickable(false);
			wholder.wselection.setVisibility(View.GONE);
						
			

			} else  {
				
                
				ServiceList wh = (ServiceList) items.get(position);

				wholder.wselection
						.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub

								CheckBox cb = (CheckBox) v;
								ServiceList wether = (ServiceList) cb.getTag();
								String name = wether.getName();
								
								boolean flag = false;

								Cursor c = null ;
								try {
									db.open();
									 c = db.getAllServiceList();
									if (c.moveToFirst()) {
										do {
											
											if (c.getString(1).compareTo(name) == 0) {
												flag = true;
											}
											
										} while (c.moveToNext());
									}
								} catch (Exception e) {
									System.out.println(e);
								}
								db.close();

								
					
								try {

									db.open();

									if (flag == true) {
										
										if(c.getCount()>2) {
										
										boolean ca = db.deleteServiceList(name);
										ServiceFragment.items.removeAll(items);
										
										ServiceFragment.servicelist.add(new ServiceList(wether.getName(),"off"));
											
											
										for(int i=0;i<ServiceFragment. servicelistListed .size();i++){
											if(ServiceFragment. servicelistListed .get(i).getName().compareTo(name)==0){
												ServiceFragment. servicelistListed .remove(ServiceFragment. servicelistListed .get(i));
											}
										}
									
									  }	else if(c.getCount() ==2){	
											Toast.makeText(
													getContext(),
													"Service cannot be less than two",
													Toast.LENGTH_LONG).show();
											temp=1;
										  }
										
										
									} else {
										if(c.getCount()<=4) {
										db.insertServiceList(name);
										ServiceFragment.items.removeAll(items);
										
										ServiceFragment. servicelistListed .add(new ServiceList(wether.getName(),"on"));
											
										for(int i=0;i<ServiceFragment.servicelist.size();i++){
											if(ServiceFragment.servicelist.get(i).getName().compareTo(name)==0){
												ServiceFragment.servicelist.remove(ServiceFragment.servicelist.get(i));
											}
										}
										}else{
											
											Toast.makeText(
													getContext(),
													"Servicelist cannot be more than five",
													Toast.LENGTH_LONG).show();
											temp=1;
									  }
									}

								} catch (Exception e) {
									System.out.println(e);
								}
		 				db.close();

					
					
		 	       if(temp==0){ 	
		 				ServiceFragment.items.add(new SectionItem("Added"));
		 	 			for(int i=0;i<ServiceFragment. servicelistListed .size();i++){
		 	 				ServiceFragment.items.add(new ServiceList(ServiceFragment. servicelistListed .get(i).getName(),"on"));
		 	 			}


		 	 			ServiceFragment.items.add(new SectionItem("Not Added"));
		 	 			for(int i=0;i<ServiceFragment.servicelist.size();i++){
		 	 				ServiceFragment.items.add(new ServiceList(ServiceFragment.servicelist.get(i).getName(),"off"));
		 	 			}
	
							
		 	 			ServiceFragment.adapter.notifyDataSetChanged();	
						
							
							}else{
								temp=0;
							}		
						}	
						});
				
				
				wholder.wname.setText(wh.appinfo);
				wholder.wname.setTextColor(Color.WHITE);
				wholder.wname.setTextSize(16);
//						.setImageDrawable(wh.appinfo.loadIcon(packageManager));
				if(wh.getState().compareTo("on")==0){
				wholder.wselection.setChecked(true);
				}
				if(wh.getState().compareTo("off")==0){
					wholder.wselection.setChecked(false);
				}
				
				wholder.wselection.setTag(i);
			}

			

		return v;
	}

	static class WHolder {

		TextView wname;
		ImageView wimage;
		CheckBox wselection;

	}


}
