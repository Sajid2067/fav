package honesty.android.section.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Sajid khan on 6/10/2016.
 */
public class TabChangedCount {
    private long tabChanged;


    private Context context;
    private SharedPreferences mPrefs;
    public TabChangedCount(Context context) {

        this.context = context;
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public SharedPreferences getPrefs() {
        return mPrefs;
    }

    public long getTabChanged() {
        return mPrefs.getLong("tabChanged", 0);
    }

    public void setTabChanged( long tabChanged) {
        mPrefs.edit().putLong("tabChanged", tabChanged).commit();
    }

    public TabChangedCount() {
    }

}
