package honesty.android.section.model;

import java.util.ArrayList;

import honesty.android.fragments.ApplicationFragment;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import honesty.android.favourite.Item;
import honesty.android.favourite.database.DBAdapterAppList;

public class AppAdapter extends ArrayAdapter<Item> {

	private Context context;
	private ArrayList<Item> items;
	private LayoutInflater vi;
	private PackageManager packageManager;
	DBAdapterAppList db;
	Description ei;
	Contacts co;
	CheckBox selection;
	
	int j=0;
	int temp=0;	
	
	public AppAdapter(Context context, ArrayList<Item> items, ListView lst) {
		super(context, 0, items);
		this.context = context;
		this.items = items;
		
		packageManager = context.getPackageManager();
		vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		db = new DBAdapterAppList(context);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;

		WHolder wholder;
		
		final Item i = items.get(position);

			
			
			  wholder = new WHolder();
			  v = vi.inflate(honesty.android.favourite.R.layout.wether_list_row, null);
				wholder.wname = (TextView) v.findViewById(honesty.android.favourite.R.id.app_name);
		        wholder.wname.setTextSize(20);
		        wholder.wname.setTextColor(Color.WHITE);
				wholder.wimage = (ImageView) v.findViewById(honesty.android.favourite.R.id.app_icon);
				wholder.wselection = (CheckBox) v
						.findViewById(honesty.android.favourite.R.id.checkBoxSelection);
				
				v.setTag(wholder);
				
		
				
			if (i.isSection() ) {
				
				
				SectionItem si = (SectionItem) i;
				wholder.wname.setText(si.getTitle());
				wholder.wname.setTextSize(20);
				wholder.wname.setTypeface(null, Typeface.BOLD);
				wholder.wname.setGravity(Gravity.CENTER_HORIZONTAL);
				wholder.wname.setTextColor(Color.WHITE);
				
				v.setOnClickListener(null);
				v.setOnLongClickListener(null);
				v.setLongClickable(false);
			wholder.wselection.setVisibility(View.GONE);
						
			

			} else  {
				
                
				Description wh = (Description) items.get(position);

				wholder.wselection
						.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub

								CheckBox cb = (CheckBox) v;
								Description description = (Description) cb.getTag();
								String pakName = description.getName().packageName;
								String appName = description.getName().loadLabel(packageManager).toString();
								
								boolean flag = false;

								Cursor c = null ;
								try {
									db.open();
									 c = db.getAllAppList();
									if (c.moveToFirst()) {
										do {
											String databasePakName=c.getString(2);
											if (databasePakName.compareTo(pakName) == 0) {
												flag = true;
											}
											
										} while (c.moveToNext());
									}
								} catch (Exception e) {
									System.out.println(e);
								}
								db.close();

								
								
								try {

									db.open();
									int dbSize=c.getCount();
									if (flag == true) {
										
											
										ApplicationFragment.items.add(new SectionItem("Added"));
						 	 			for(int i=0;i<ApplicationFragment.applistListed.size();i++){
						 	 				ApplicationFragment.items.add(new Description(ApplicationFragment.applistListed.get(i),true));
						 	 			}


						 	 			ApplicationFragment.items.add(new SectionItem("Not Added"));
						 	 			for(int i=0;i<ApplicationFragment.applist.size();i++){
						 	 				ApplicationFragment.items.add(new Description(ApplicationFragment.applist.get(i),false));
						 	 			}


//								 	 			ServiceActivity.adapter.notifyDataSetChanged();

								 	 if(c.getCount()>2) {
										boolean ca = db.deleteAppList(pakName);
										ApplicationFragment.items.removeAll(items);

											ApplicationFragment.applist.add(description.getName());


										for(int i=0;i<ApplicationFragment.applistListed.size();i++){
											if(ApplicationFragment.applistListed.get(i).packageName.compareTo(pakName)==0){
											ApplicationFragment.applistListed.remove(ApplicationFragment.applistListed.get(i));
											}
										}

									}else if(c.getCount() ==2){
										Toast.makeText(
											getContext(),
											"Applist cannot be less than two",
											Toast.LENGTH_LONG).show();
										temp=1;
									 }
									} else {
										if(c.getCount()<=4) {
										db.insertAppList(pakName,appName);
										ApplicationFragment.items.removeAll(items);

											ApplicationFragment.applistListed.add(description.getName());

										for(int i=0;i<ApplicationFragment.applist.size();i++){
											if(ApplicationFragment.applist.get(i).packageName.compareTo(pakName)==0){
											ApplicationFragment.applist.remove(ApplicationFragment.applist.get(i));
											}
										}
										}else{

											Toast.makeText(
													getContext(),
													"Applist cannot be more than five",
													Toast.LENGTH_LONG).show();
											temp=1;
									}
									}

								} catch (Exception e) {
									System.out.println(e);
								}
		 				db.close();


		 		if(temp==0){
					if(ApplicationFragment.applist ==null){
						ApplicationFragment.applist = new ArrayList<ApplicationInfo>();
					}
					if(ApplicationFragment.applistListed ==null){
						ApplicationFragment.applistListed = new ArrayList<ApplicationInfo>();
					}
					if(ApplicationFragment.items==null){
						ApplicationFragment.items = new ArrayList<Item>();
					}

		 	 	    	ApplicationFragment.items.add(new SectionItem("Added"));
		 	 			for(int i=0;i<ApplicationFragment.applistListed.size();i++){
		 	 				ApplicationFragment.items.add(new Description(ApplicationFragment.applistListed.get(i),true));
		 	 			}


		 	 			ApplicationFragment.items.add(new SectionItem("Not Added"));
		 	 			for(int i=0;i<ApplicationFragment.applist.size();i++){
		 	 				ApplicationFragment.items.add(new Description(ApplicationFragment.applist.get(i),false));
		 	 			}
	
							
						ApplicationFragment.adapter.notifyDataSetChanged();	
							
								
						 
						}else{
							temp=0;
						}
		 		
							}
						
						});
				
				
				wholder.wname.setText(wh.appinfo.loadLabel(packageManager));
				wholder.wname.setTextSize(16);
				wholder.wimage
						.setImageDrawable(wh.appinfo.loadIcon(packageManager));
				wholder.wselection.setChecked(wh.isSelected());
				wholder.wselection.setTag(i);

		}

		

		return v;
	}

	public static class WHolder {

		TextView wname;
		ImageView wimage;
		CheckBox wselection;

	}


}
