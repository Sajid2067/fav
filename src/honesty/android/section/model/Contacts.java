package honesty.android.section.model;

import android.content.pm.ApplicationInfo;

import honesty.android.favourite.Item;

public class Contacts implements Item {

	ApplicationInfo appinfo;
	

	public Contacts(ApplicationInfo appinfo) {
		this.appinfo = appinfo;
		
	}
	
	@Override
	public boolean isSection() {
		return false;
	}

	@Override
	public boolean isDescription() {
		return false;
	}
}