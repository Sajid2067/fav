package honesty.android.Settings;

import honesty.android.favourite.Item;


public class SettingsItem implements Item {

	
	public String option;
	boolean selected = false;

	public SettingsItem(String option,boolean selected) {
		
		this.option = option;
		this.selected=selected;
	}
	
	@Override
	public boolean isSection() {
		return false;
	}

	@Override
	public boolean isDescription() {
		return true;
	}
	
	 public boolean isSelected() {  
		 return selected;  
		 } 
	 
	 public void setSelected(boolean selected) { 
		 this.selected = selected;  
		 } 
	 
	 public String getName() {   return option;  } 

}
