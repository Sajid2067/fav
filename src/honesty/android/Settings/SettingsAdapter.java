package honesty.android.Settings;

import java.util.ArrayList;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import honesty.android.section.model.Contacts;
import honesty.android.section.model.Description;
import honesty.android.favourite.Item;
import honesty.android.favourite.database.DBAdapterAppList;

public class SettingsAdapter extends ArrayAdapter<Item> {

	private Context context;
	private ArrayList<Item> items;
	private LayoutInflater vi;
	private PackageManager packageManager;
	DBAdapterAppList db;
	Description ei;
	Contacts co;
	CheckBox selection;
	
	int j=0;
	int temp=0;	
	
	  SharedPreferences sharedpreferences;
	   public static final String MyPREFERENCES = "MyPrefs" ;
	 	   Editor editor;
	
	public SettingsAdapter(Context context, ArrayList<Item> items, ListView lst) {
		super(context, 0, items);
		this.context = context;
		this.items = items;
		
		packageManager = context.getPackageManager();
		vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		sharedpreferences=PreferenceManager.getDefaultSharedPreferences(context);
		editor = sharedpreferences.edit();
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;

		WHolder wholder;
		
		final Item i = items.get(position);
	
			  wholder = new WHolder();
			  v = vi.inflate(honesty.android.favourite.R.layout.settings_list_row, null);
				wholder.wname = (TextView) v.findViewById(honesty.android.favourite.R.id.settings_name);
		        wholder.wname.setTextColor(Color.WHITE);
		        wholder.wname.setTextSize(16);
				wholder.wselection = (CheckBox) v
						.findViewById(honesty.android.favourite.R.id.checkBoxSettings);
				
				v.setTag(wholder);
		               
				SettingsItem wh = (SettingsItem) items.get(position);

				wholder.wselection
						.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub

								CheckBox cb = (CheckBox) v;
								boolean state=cb.isChecked();
								
								if(state==true){
									 editor.putString("bootStatus", "1"); 
									 Toast.makeText(
												getContext(),
												"Floater will start if device re-start",
												Toast.LENGTH_LONG).show();
								}else{
									 editor.putString("bootStatus", "0"); 
									 Toast.makeText(
												getContext(),
												"Floater will not start if device re-start",
												Toast.LENGTH_LONG).show();
								}
								editor.commit();
							 }
							});
				
				
				wholder.wname.setText(wh.option);

				if(wh.option.compareTo("Start on re-start")==0){
					wholder.wselection.setVisibility(View.VISIBLE);
					 String	boot=sharedpreferences.getString("bootStatus", "0");
					 if(boot.compareTo("0")==0){
					wholder.wselection.setChecked(false);
					 }else{
						 wholder.wselection.setChecked(true); 
					 }
				}else{
					wholder.wselection.setVisibility(View.INVISIBLE);
				}
				
				wholder.wselection.setTag(i);
	

		return v;
	}

	static class WHolder {

		TextView wname;
		CheckBox wselection;

	}

}
