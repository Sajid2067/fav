package honesty.android.Settings;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.content.SharedPreferences.Editor;

import honesty.android.favourite.HUD;
import honesty.android.favourite.R;

public class ButtonSizeActivity extends Activity {

	private SeekBar seekBar;
	private TextView textView;
	SharedPreferences sharedpreferences;
	public static final String MyPREFERENCES = "MyPrefs" ;
	Editor editor;
	Button ok;
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_seekbar);
		sharedpreferences= PreferenceManager.getDefaultSharedPreferences(ButtonSizeActivity.this);
		editor = sharedpreferences.edit();
		initializeVariables();

		// Initialize the textview with '0'.
		textView.setText("Covered: " + seekBar.getProgress() + "/" + seekBar.getMax());

		seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			int progress = 0;

			@Override
			public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
				progress = progresValue;
				//Toast.makeText(getApplicationContext(), "Changing seekbar's progress", Toast.LENGTH_SHORT).show();
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				//Toast.makeText(getApplicationContext(), "Started tracking seekbar", Toast.LENGTH_SHORT).show();
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				textView.setText("Covered: " + progress + "/" + seekBar.getMax());
				//Toast.makeText(getApplicationContext(), "Stopped tracking seekbar", Toast.LENGTH_SHORT).show();

				editor.putInt("bootIconSize", progress);
				editor.commit();

				stopService(new Intent(ButtonSizeActivity.this, HUD.class));
				Intent svc = new Intent(ButtonSizeActivity.this, HUD.class);
				startService(svc);
			}
		});
	}

	// A private method to help us initialize our variables.
	private void initializeVariables() {
		seekBar = (SeekBar) findViewById(R.id.seekBar1);
		int	strProgress=sharedpreferences.getInt("bootIconSize", 1);
		seekBar.setProgress(strProgress);
		textView = (TextView) findViewById(R.id.textView1);
		ok = (Button)findViewById(R.id.buttonOk);
		ok.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
}