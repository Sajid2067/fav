package honesty.android.Settings;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.ListView;
import honesty.android.section.model.AppAdapter;
import honesty.android.favourite.HUD;
import honesty.android.favourite.ImageAdapter;
import honesty.android.favourite.Item;
import honesty.android.favourite.R;
import honesty.android.favourite.database.DBAdapterAppList;

public class ButtonChangeActivity extends Activity {
	
	 public static ArrayList<Item> items ;
	  private PackageManager packageManager = null;
	    public static List<ApplicationInfo> applist = null;
	    public  static List<ApplicationInfo> applistListed = null;
	    DBAdapterAppList db;
	   public static AppAdapter adapter;
	    ListView l1,l2;
	    ImageView selected,img1;;
	    ImageView selectedImage; 
	     private Integer[] mImageIds = {
	                R.drawable.image1,
	                R.drawable.image2,
	                R.drawable.image3,
	                R.drawable.image4,
	                R.drawable.image5,
	                R.drawable.image6
	              
	        };
	   public int tempPosition=0;
	     
	     SharedPreferences sharedpreferences;
		   public static final String MyPREFERENCES = "MyPrefs" ;
		 	   Editor editor;
		 Button ok,back;	   
	 @SuppressWarnings("deprecation")
		@Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.iconsettings);
	      
	        sharedpreferences=PreferenceManager.getDefaultSharedPreferences(this);
			editor = sharedpreferences.edit();

			back=(Button)findViewById(R.id.button2);
			
	        Gallery gallery = (Gallery) findViewById(R.id.gallery1);
	        selectedImage=(ImageView)findViewById(R.id.imageView1);
	        
	        int	boot=sharedpreferences.getInt("bootIconNumber", 0);
			
		 if(boot==0){
		//	 Picasso.with(ButtonChangeActivity.this).load(R.drawable.image1).into(selectedImage);
			 selectedImage.setImageResource(R.drawable.image1);
			}else if(boot==1){
			// Picasso.with(ButtonChangeActivity.this).load(R.drawable.image2).into(selectedImage);
			 selectedImage.setImageResource(R.drawable.image2);
			}else if(boot==2){
			 selectedImage.setImageResource(R.drawable.image3);
		//	 Picasso.with(ButtonChangeActivity.this).load(R.drawable.image3).into(selectedImage);
			}else if(boot==3){
			 selectedImage.setImageResource(R.drawable.image4);
		//	 Picasso.with(ButtonChangeActivity.this).load(R.drawable.image4).into(selectedImage);
			}else if(boot==4){
	//		 Picasso.with(ButtonChangeActivity.this).load(R.drawable.image5).into(selectedImage);
			 selectedImage.setImageResource(R.drawable.image5);
			}else if(boot==5){
		//	 Picasso.with(ButtonChangeActivity.this).load(R.drawable.image6).into(selectedImage);
			 selectedImage.setImageResource(R.drawable.image6);
			}else{
			// Picasso.with(ButtonChangeActivity.this).load(R.drawable.icons).into(selectedImage);
			 selectedImage.setImageResource(R.drawable.icons);
			}
	        
	        gallery.setSpacing(5);
	        gallery.setAdapter(new ImageAdapter(this));
	        gallery.setSelection(1);
	     //   MarginLayoutParams mlp=(MarginLayoutParams)gallery.getLayoutParams();

	     //  mlp.setMargins(30, 0, 0, 0);
	        
	        
	         // clicklistener for Gallery
	        gallery.setOnItemClickListener(new OnItemClickListener() {
	           
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
					//  Toast.makeText(IconActivity.this, "Your selected position = " + position, Toast.LENGTH_SHORT).show();
		                // show the selected Image
					  selectedImage.setImageResource(mImageIds[position]);
				//	Picasso.with(ButtonChangeActivity.this).load(mImageIds[position]).into(selectedImage);
					//  tempPosition=position;
					  editor.putInt("bootIconNumber", position); 
						  editor.commit();
						//stoping itself     
						stopService(new Intent(ButtonChangeActivity.this, HUD.class));
						  Intent svc = new Intent(ButtonChangeActivity.this, HUD.class);
						startService(svc);
		              
				}
	        });
	               
	   
	        
       back.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					finish();

				}
			});
	        
	        
	    }
}
