/**
 * 
 */
package honesty.android.favourite.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * @author sajid.khan
 * 
 */

// consists of 2 tables : purchase,sales

public class DBAdapterAppList {
	// 3 TABLES : PURCHASE,SALES
	private static final String TAG = "appList";
	private static final String DATABASE_NAME = "appserviceList";
	// private static final String DATABASE_TABLE = "products";
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_TABLE_APPlIST = "appList";
	private static final String DATABASE_TABLE_SERVICElIST = "serviceList";

	// variables for APPLIST table
	public static final String KEY_APPLIST_ID = "applistid";
	public static final String KEY_APPLIST_PACKAGE = "applistpackage";
	public static final String KEY_APPLIST_NAME = "applistname";
	
	
	// variables for SERVICE table
		public static final String KEY_SERVICElIST_ID = "serviceid";
		public static final String KEY_SERVICElIST_NAME = "servicename";
		public static final String KEY_SERVICElIST_STATE = "servicestate";

	private static final String TABLE_CREATE_APPlIST = "CREATE TABLE "
			+ DATABASE_TABLE_APPlIST + "(" + KEY_APPLIST_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," 
			+ KEY_APPLIST_NAME + " TEXT,"
			+ KEY_APPLIST_PACKAGE + " TEXT " + ")";
	
	
	private static final String TABLE_CREATE_SERVICElIST = "CREATE TABLE "
			+ DATABASE_TABLE_SERVICElIST + "(" + KEY_SERVICElIST_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," 
			+ KEY_SERVICElIST_NAME + " TEXT,"
			+ KEY_SERVICElIST_STATE + " TEXT " + ")";

	// end of variable of purchase products

	// variables for sales tables



	private final Context context;
	private DatabaseHelper DBHelper;
	private SQLiteDatabase db;

	public DBAdapterAppList(Context ctx) {
		this.context = ctx;
		DBHelper = new DatabaseHelper(context);
	}

	private static class DatabaseHelper extends SQLiteOpenHelper {
		DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(TABLE_CREATE_APPlIST);
			db.execSQL(TABLE_CREATE_SERVICElIST);
			
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
					+ newVersion + ", which will destroy all old data");
			db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_APPlIST);
			db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_SERVICElIST);
			onCreate(db);
		}
	}

	// ---opens the database---
	public DBAdapterAppList open() throws SQLException {
		db = DBHelper.getWritableDatabase();
		return this;
	}

	// ---closes the database---
	public void close() {
		DBHelper.close();
	}

	// ---insert a apppackage into the database---
	public long insertAppList(String apppackage,String appName) {

		ContentValues initialValues = new ContentValues();

		initialValues.put(KEY_APPLIST_PACKAGE, apppackage);
		initialValues.put(KEY_APPLIST_NAME, appName);
		

		return db.insert(DATABASE_TABLE_APPlIST, null, initialValues);
	}
	
	
	// ---insert a service into the database---
		public long insertServiceList(String service) {

			ContentValues initialValues = new ContentValues();

			initialValues.put(KEY_SERVICElIST_NAME, service);
			//initialValues.put(KEY_SERVICElIST_STATE, state);
			
			

			return db.insert(DATABASE_TABLE_SERVICElIST, null, initialValues);
		}
	
	
	// ---deletes a particular apppackage---
	public boolean deleteAppList(String apppackage) {

		String where = "applistpackage=?";
		String[] whereArgs = new String[] {apppackage};

		return db.delete(DATABASE_TABLE_APPlIST, where, whereArgs) > 0;

	
	}
	
	
	// ---deletes a particular service---
		public boolean deleteServiceList(String service) {

			String where = "servicename=?";
			String[] whereArgs = new String[] {service};

			return db.delete(DATABASE_TABLE_SERVICElIST, where, whereArgs) > 0;

		
		}

	
	
	// ---retrieves all the appList---
	public Cursor getAllAppList() {
		return db.query(DATABASE_TABLE_APPlIST, new String[] {

		}, null, null, null, null, null);
	}
	
	
	// ---retrieves all the service---
		public Cursor getAllServiceList() {
			return db.query(DATABASE_TABLE_SERVICElIST, new String[] {

			}, null, null, null, null, null);
		}
	
	
	
	// ---updates a service state---
		public boolean updatePurchase(String service,String state) {

			String where = "servicename=?";
			String[] whereArgs = new String[] { service };
			ContentValues args = new ContentValues();

			args.put(KEY_SERVICElIST_STATE, state);

			return db.update(DATABASE_TABLE_SERVICElIST, args, where, whereArgs) > 0;
		}

	
	

}
