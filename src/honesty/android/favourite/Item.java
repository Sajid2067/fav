package honesty.android.favourite;

public interface Item {
	
	public boolean isSection();
	public boolean isDescription();

}
