package honesty.android.favourite;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;



public class CallActivity extends Activity {
	
	Button btnOk;
	EditText edtInputNumber;
	TextView txtAlreadyListed,txtAlreadyListedNumber,txtNewNumber;
	CheckBox addDelete;
	
	   SharedPreferences sharedpreferences;
	   public static final String MyPREFERENCES = "MyPrefs" ;
	   public static final String Phone = "phoneCallNumber"; 
	   public static final String PhoneState = "phoneCallNumberState"; 
	   Editor editor;
	   String number;
	   
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.calllayout);
		
		btnOk=(Button)findViewById(R.id.buttonOk);
		edtInputNumber=(EditText)findViewById(R.id.edittextInputNumber);
		txtAlreadyListed=(TextView)findViewById(R.id.textViewAlreadyListed);
		txtAlreadyListedNumber=(TextView)findViewById(R.id.textViewAlreadyListedNumber);
		txtNewNumber=(TextView)findViewById(R.id.textViewSetNewNumber);
		addDelete=(CheckBox)findViewById(R.id.checkBoxListing);
		
		sharedpreferences=PreferenceManager.getDefaultSharedPreferences(this);
		editor = sharedpreferences.edit();
        
		number=sharedpreferences.getString("phoneCallNumber", "0");
		
		String numberStatus=sharedpreferences.getString("phoneCallNumberState", "0");
		
		
		 if(numberStatus.compareTo("0")==0 || numberStatus.compareTo("excluded")==0){
			addDelete.setChecked(false);
			addDelete.setText("Add to Floater?");
		 }else{
			addDelete.setChecked(true);
			addDelete.setText("Delete from Floater?");
		   }
		
		
		txtAlreadyListedNumber.setTextColor(Color.BLUE);
		
		if(number.compareTo("0")==0){
			txtAlreadyListedNumber.setText("No number saved");
			
		}else{
			txtAlreadyListedNumber.setText(number);
		}
		
        addDelete.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				number=sharedpreferences.getString("phoneCallNumber", "0");
				
				if(number.compareTo("0")==0 || number.compareTo("")==0){
					Toast.makeText(getApplicationContext(),"Please Insert Number !!!", Toast.LENGTH_LONG).show();
				}else{
				if(isChecked==true){
			      editor.putString(PhoneState, "included");
			      addDelete.setText("Delete from Floater?");
			      Toast.makeText(getApplicationContext(), "Included successfully", Toast.LENGTH_LONG).show();
				  }else{
					  editor.putString(PhoneState, "excluded"); 
					  addDelete.setText("Add to Floater?");
					  Toast.makeText(getApplicationContext(), "Excluded successfully", Toast.LENGTH_LONG).show();
				  }
			      editor.commit(); 
				}  
			
			}
		});
				
		btnOk.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
			    String ph=edtInputNumber.getText().toString();
			    if(ph.compareTo("")==0){
			    	Toast.makeText(getApplicationContext(), "Please Insert Number!!!", Toast.LENGTH_LONG).show();
			    }else{
				
				AlertDialog alertDialogCategory = new AlertDialog.Builder(
						CallActivity.this)
						.setTitle("Set this number for direct Call ?")

						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface arg0,
											int arg1) {
										// TODO Auto-generated method stub

										
									     String ph=edtInputNumber.getText().toString();
									      editor.putString(Phone, ph);
									      editor.commit(); 	
									Toast.makeText(getApplicationContext(), "Number saved successfully", Toast.LENGTH_LONG).show();
									
									txtAlreadyListedNumber.setText(ph);
									edtInputNumber.setText("");
//									finish();
//									Intent i=new Intent(CallActivity.this,CallActivity.class);
//									startActivity(i);

									}

								})
						.setNegativeButton("Cancel",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub

									}

								}).create();

				alertDialogCategory.show();
			    }
				
				
			}
		});
	}

}
