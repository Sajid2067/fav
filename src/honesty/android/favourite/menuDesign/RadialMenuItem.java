package honesty.android.favourite.menuDesign;

import java.util.List;

import android.util.Log;

/**
 * This class handles the menu item creation.
 * 
 */
public class RadialMenuItem implements RadialMenuInterface {
	private String mMenuID;
	private String mMenuName;
	private String menuName = "Empty";
//	private String menuName = "Expandable";
	private String menuLabel = null;
	private int menuIcon = 0;
	private List<RadialMenuItem> menuChildren = null;
	private RadialMenuItemClickListener menuListener = null;
	private RadialMenuRenderer.OnRadailMenuClick mCallback;
	public static String activatedMenu="Apps";
	/**
	 * Creates an instance of the RadialMenuItem.
	 * @param name - (String) If there is no name to be assigned pass null. (Name is mostly used for debugging)
	 * @param displayName - (String) If there is no display name to be assigned pass null.
	 */
	/**
	 * @param mMenuID
	 * @param mMenuName
	 */
	public RadialMenuItem(String mMenuID, String mMenuName) {
		this.mMenuID = mMenuID;
		this.menuName= mMenuID;
		this.mMenuName = mMenuName;
		this.menuLabel = mMenuName;
	}
	
	/**
	 * @return the mMenuID
	 */
	public String getMenuID() {
		return mMenuID;
	}
	
	
	/**
	 * Set menu item icon.
	 * @param displayIcon - (int) Icon resource ID.
	 * <strong>secondChildItem.setDisplayIcon(R.drawable.ic_launcher);</strong>
	 */
	public void setDisplayIcon(int displayIcon) {
		this.menuIcon = displayIcon;
		
	}
	
	/**
	 * Set the on menu item click event.
	 * @param listener
	 */
	public void setOnMenuItemPressed(RadialMenuItemClickListener listener) {
	
		menuListener = listener;
	
	}
	
	/**
	 * @return the mMenuName
	 */
	public String getMenuName() {
	
		return mMenuName;
	}
	
	
	/**
	 * Set the menu child items.
	 * @param childItems - Pass the list of child items.
	 */
	public void setMenuChildren(List<RadialMenuItem> childItems) {
		
		this.menuChildren = childItems;
	
	}
	
	/**
	 * 
	 * @param onRadailMenuClick
	 */
	public void setOnRadialMenuClickListener(RadialMenuRenderer.OnRadailMenuClick onRadailMenuClick) {
		
		this.mCallback = onRadailMenuClick;
	
	}
	
	
	@Override
	public String getName() {
		return menuName;
	}

	@Override
	public String getLabel() {
		
		return menuLabel;
	}

	@Override
	public int getIcon() {  //RadialMenuWidget.java
	
		return menuIcon;
		
	}

	@Override
	public List<RadialMenuItem> getChildren() {
		
		return menuChildren;
		
	}

	
	@Override
	public void menuActiviated() {
		
		Log.i(this.getClass().getName(), menuName + " menu pressed.");
		activatedMenu=menuName;
		//menuListener=null;
//		if(menuListener != null)
//			menuListener.execute();
//		 if(menuName.contains("Close")){
//
//		 }
		
		}
	
	
	public interface RadialMenuItemClickListener {
		public void execute();
	}
	
	/**
	 * 
	 * @return
	 */
	public RadialMenuRenderer.OnRadailMenuClick getOnRadailMenuClick() {
		
		return mCallback;
	}
	
	
	
	
}
