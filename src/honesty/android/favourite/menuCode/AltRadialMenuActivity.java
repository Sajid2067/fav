package honesty.android.favourite.menuCode;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;

import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import honesty.android.section.model.ServiceList;
import honesty.android.favourite.HUD;
import honesty.android.favourite.R;
import honesty.android.favourite.database.DBAdapterAppList;
import honesty.android.favourite.menuDesign.RadialMenuItem;
import honesty.android.favourite.menuDesign.RadialMenuWidget;

public class AltRadialMenuActivity extends Activity {

    private RadialMenuWidget pieMenu;

    static String strtext;
    public RadialMenuItem serviceItem, menuCloseItem, appItem;
    public RadialMenuItem secondChildItem, normalChildItem;
    private List<RadialMenuItem> appItemChildren = new ArrayList<RadialMenuItem>();
    private List<RadialMenuItem> serviceItemChildren = new ArrayList<RadialMenuItem>();
    private View vi;
    protected Intent Intent;
    DBAdapterAppList db;
    private PackageManager packageManager = null;
    public static List<ApplicationInfo> applist = null;
    public static List<ServiceList> serviceList = null;
    String appName;
    String tempPackageName;
    SharedPreferences sharedpreferences;
    Editor editor;
    public static final String MyPREFERENCES = "MyPrefs";
    public static final String Phone = "phoneMessageNumber";
    public static final String PhoneState = "phoneMessageNumberState";


    @SuppressWarnings("serial")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Check the OS and set the app bar likewise

        db = new DBAdapterAppList(this);

        WindowManager.LayoutParams params = getWindow().getAttributes();

        sharedpreferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sharedpreferences.edit();

        this.getWindow().setAttributes(params);



        pieMenu = new RadialMenuWidget(this);

        menuCloseItem = new RadialMenuItem(getString(honesty.android.favourite.R.string.close), null);
        menuCloseItem
                .setDisplayIcon(R.drawable.home2);

        serviceItem = new RadialMenuItem(getString(honesty.android.favourite.R.string.services),
                getString(honesty.android.favourite.R.string.services));

        appItem = new RadialMenuItem(getString(honesty.android.favourite.R.string.apps),
                getString(honesty.android.favourite.R.string.apps));

        menuCloseItem
                .setOnMenuItemPressed(new RadialMenuItem.RadialMenuItemClickListener() {
                    @Override
                    public void execute() {
                        // menuLayout.removeAllViews();

                    }
                });

        // pieMenu.setDismissOnOutsideClick(true, menuLayout);
        pieMenu.setAnimationSpeed(10);
        pieMenu.setSourceLocation(0, 0); //Location of the Menu where it will draw
        pieMenu.setIconSize(30, 50);
        pieMenu.setTextSize(13);
        pieMenu.setOutlineColor(Color.RED, 225);
        pieMenu.setInnerRingColor(R.color.radialFirstColor, 180);
        pieMenu.setOuterRingColor(R.color.tabColor, 180);
        //pieMenu.setHeader("Test Menu", 20);
        pieMenu.setCenterCircle(menuCloseItem);

        //Adding Customized App

        String messageNumber = sharedpreferences.getString("phoneMessageNumberState", "0");
        String callNumber = sharedpreferences.getString("phoneCallNumberState", "0");

        if (messageNumber.compareTo("included") == 0) {
            secondChildItem = new RadialMenuItem("Favorites Message",
                    "Favorites Message");

            secondChildItem.setDisplayIcon(honesty.android.favourite.R.drawable.ic_launcher);

            secondChildItem
                    .setOnMenuItemPressed(new RadialMenuItem.RadialMenuItemClickListener() {
                        @Override
                        public void execute() {

                        }

                    });

            appItemChildren.add(secondChildItem);
        }
        if (callNumber.compareTo("included") == 0) {
            secondChildItem = new RadialMenuItem("Favorites Call",
                    "Favorites Call");
            secondChildItem.setDisplayIcon(honesty.android.favourite.R.drawable.ic_launcher);

            secondChildItem
                    .setOnMenuItemPressed(new RadialMenuItem.RadialMenuItemClickListener() {
                        @Override
                        public void execute() {

                        }
                    });

            appItemChildren.add(secondChildItem);

        }

        packageManager = getPackageManager();
        applist = checkForLaunchIntent(packageManager.getInstalledApplications(PackageManager.GET_META_DATA));

        try {
            db.open();
            Cursor c = db.getAllAppList();
            if (c.moveToFirst()) {
                do {

                    String packageName = c.getString(2);
                    boolean deleted=true;
                    for (int i = 0; i < applist.size(); i++) {
                        if (applist.get(i).packageName.compareTo(packageName) == 0) {
                           deleted=false;
                        }
                    }
                    if(deleted==true){
                        db.deleteAppList(packageName);
                    }

                } while (c.moveToNext());
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        db.close();



        try {
            db.open();
            Cursor c = db.getAllAppList();
            if (c.moveToFirst()) {
                do {

                    String packageName = c.getString(2);

                    for (int i = 0; i < applist.size(); i++) {
                        if (applist.get(i).packageName.compareTo(packageName) == 0) {
                            tempPackageName = packageName;
                            appName = applist.get(i).loadLabel(packageManager).toString();
                            secondChildItem = new RadialMenuItem(appName,
                                    appName);
                            secondChildItem.setDisplayIcon(honesty.android.favourite.R.drawable.ic_launcher);
                            appItemChildren.add(secondChildItem);
                        }
                    }

                } while (c.moveToNext());
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        db.close();


        serviceList = new ArrayList<ServiceList>();

        serviceList.add(new ServiceList("WI-FI", "off"));
        serviceList.add(new ServiceList("BLUETOOTH", "off"));
        serviceList.add(new ServiceList("ROTATION", "off"));
        serviceList.add(new ServiceList("DATA", "off"));
        serviceList.add(new ServiceList("HOTSPOT", "off"));
        serviceList.add(new ServiceList("SOUND", "off"));
        serviceList.add(new ServiceList("GPS", "off"));
        serviceList.add(new ServiceList("NFC", "off"));

        try {
            db.open();
            Cursor c = db.getAllServiceList();
            if (c.moveToFirst()) {
                do {

                    String packageName = c.getString(1);
                    for (int i = 0; i < serviceList.size(); i++) {
                        if (serviceList.get(i).getName().compareTo(packageName) == 0) {
                            // tempPackageName=packageName;
                            appName = serviceList.get(i).getName();
                        }
                    }

                    normalChildItem = new RadialMenuItem(appName,
                            appName);
                    normalChildItem.setDisplayIcon(honesty.android.favourite.R.drawable.ic_launcher);
                    normalChildItem
                            .setOnMenuItemPressed(new RadialMenuItem.RadialMenuItemClickListener() {
                                @Override
                                public void execute() {
                                    // Can edit based on preference. Also can add animations
                                    // here.

                                    String a = normalChildItem.activatedMenu;

                                    Toast.makeText(getApplicationContext(), a, Toast.LENGTH_LONG).show();
                                    //Toast.makeText(getApplicationContext(), id, Toast.LENGTH_LONG).show();
                                }
                            });


                    serviceItemChildren.add(normalChildItem);
                    // DisplayTitle(c);
                } while (c.moveToNext());
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        db.close();


        serviceItem.setMenuChildren(serviceItemChildren);
        appItem.setMenuChildren(appItemChildren);


        pieMenu.addMenuEntry(new ArrayList<RadialMenuItem>() {
            {
                add(serviceItem);
                add(appItem);
            }
        });

        new Handler().postDelayed(new Runnable() {
            public void run() {
                if (vi == null) {
                    vi = new View(AltRadialMenuActivity.this);
//				
                    pieMenu.show(vi);
                }
            }
        }, 50);


    }

    @Override
    protected void onResume() {
        super.onResume();

    }


    private void startServiceFunction() {

        Intent svc = new Intent(this, HUD.class);
        startService(svc);
    }


    //	@Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        startServiceFunction();
        onDestroy();
    }


    private List<ApplicationInfo> checkForLaunchIntent(List<ApplicationInfo> list) {
        ArrayList<ApplicationInfo> applist = new ArrayList<ApplicationInfo>();

        for (ApplicationInfo info : list) {
            try {
                if (null != packageManager.getLaunchIntentForPackage(info.packageName)) {
                    applist.add(info);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return applist;
    }


}
