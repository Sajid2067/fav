package honesty.android.favourite;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import honesty.android.favourite.menuCode.AltRadialMenuActivity;
import honesty.android.favourite.menuDesign.RadialMenuItem;
import honesty.android.favourite.menuDesign.RadialMenuWidget;

import java.util.ArrayList;
import java.util.List;

//public class HUD extends Service implements OnTouchListener {
public class HUD extends Service {
    public int flag1, flag;
    private WindowManager windowManager;
    private ImageView chatHead;
  //  TextView text;
    Button mButton;

    int initialX;
    int initialY;
    float initialTouchX;
    float initialTouchY;
    int answerX, answerY;
    static float clickTime = 0;
    static double d = 0;

    private RadialMenuWidget pieMenu;

    static String strtext;
    private FrameLayout mFragmentContainer;
    public RadialMenuItem menuItem, menuCloseItem, menuExpandItem;
    public RadialMenuItem firstChildItem, secondChildItem, thirdChildItem,
            fourthChildItem, fifthChildItem;
    private List<RadialMenuItem> children = new ArrayList<RadialMenuItem>();
    protected int click_count = 0;
    private int bootIconSize = 1;

    WindowManager.LayoutParams params;

    SharedPreferences sharedpreferences;
    Editor editor;
    public static final String MyPREFERENCES = "MyPrefs";
    public static final String xLocation = "xLocation";
    public static final String yLocation = "yLocation";

    int xLoc = 0;
    int yLoc = 0;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        sharedpreferences = PreferenceManager.getDefaultSharedPreferences(HUD.this);
        editor = sharedpreferences.edit();

        xLoc = sharedpreferences.getInt(xLocation, 0);
        yLoc = sharedpreferences.getInt(yLocation, 0);
        /////////////////////////////// Notification ///////////

        NotificationManager mNotificationManager = (NotificationManager) HUD.this.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(12345);


        int boot = sharedpreferences.getInt("bootIconNumber", 0);
        bootIconSize = sharedpreferences.getInt("bootIconSize", 1);
        ////Chathead/////

        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        chatHead = new ImageView(this);


        if (boot == 0) {
            chatHead.setImageResource(R.drawable.image1);
          //  Picasso.with(this).load(R.drawable.image1).into(chatHead);
        } else if (boot == 1) {
            chatHead.setImageResource(R.drawable.image2);
          //  Picasso.with(this).load(R.drawable.image2).into(chatHead);
        } else if (boot == 2) {
           chatHead.setImageResource(R.drawable.image3);
       //     Picasso.with(this).load(R.drawable.image3).into(chatHead);
        } else if (boot == 3) {
           chatHead.setImageResource(R.drawable.image4);
    //        Picasso.with(this).load(R.drawable.image4).into(chatHead);
        } else if (boot == 4) {
            chatHead.setImageResource(R.drawable.image5);
        //    Picasso.with(this).load(R.drawable.image6).into(chatHead);
        } else if (boot == 5) {
            chatHead.setImageResource(R.drawable.image6);
       //     Picasso.with(this).load(R.drawable.image6).into(chatHead);
        } else {
            chatHead.setImageResource(R.drawable.icons);
      //      Picasso.with(this).load(R.drawable.icons).into(chatHead);
        }



      //  text = new TextView(this);
       // text.setText("test");
        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

//		chatHead = new ImageView(this);
//		chatHead.setImageResource(R.drawable.icons);

        params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.TOP | Gravity.LEFT;
//		params.x = 0;
//		params.y = 0;
        params.height = 100 + bootIconSize * 20;
        params.width = 100 + bootIconSize * 20;
        params.x = xLoc;
        params.y = yLoc;


        windowManager.addView(chatHead, params);

        chatHead.setOnTouchListener(new View.OnTouchListener() {

            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;
            private float ox, oy, nx, ny, ax, ay;
            private int flag = 0;
            private int retflag = 0;

            // private float clickTime=0;

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        initialX = params.x;
                        initialY = params.y;
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();
                        return false;
                    case MotionEvent.ACTION_UP:
                        // Toast.makeText(getApplicationContext(),"ClickTime "
                        // +clickTime, Toast.LENGTH_SHORT).show();
                        d = Math.sqrt((initialY - params.y) * (initialY - params.y)
                                + (initialX - params.x) * (initialX - params.x));
                        clickTime = event.getEventTime() - event.getDownTime();

                        if (d < 30.0) {

                            // clickTime = event.getEventTime() -
                            // event.getDownTime();
                            if (clickTime < 135) {
                                clickTime = 0;
                                retflag = 1;
                            } else {
                                retflag = 0;

// use System.currentTimeMillis() to have a unique ID for the pending intent
                                PendingIntent pIntent = PendingIntent.getService(HUD.this, 0, new Intent(HUD.this, HUD.class), 0);

// build notification
// the addAction re-use the same intent to keep the example short
                                Notification n = new Notification.Builder(HUD.this)
                                        .setContentTitle("Favourites")
                                        .setContentText("Your Favourites is here,Please tap.")
                                        .setSmallIcon(R.drawable.icons_big)
                                        .setContentIntent(pIntent)
                                        .setAutoCancel(false)
                                        .build();


                                NotificationManager notificationManager =
                                        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                                // Cancel the notification after its selected
                                n.flags |= Notification.FLAG_AUTO_CANCEL;
                                n.flags |= Notification.FLAG_NO_CLEAR;
                                notificationManager.notify(0, n);

                                //stoping itself
                                HUD.this.stopSelf();

                            }

                        } else {

                            retflag = 0;
                        }

                    case MotionEvent.ACTION_MOVE:
                        params.x = initialX
                                + (int) (event.getRawX() - initialTouchX);
                        params.y = initialY
                                + (int) (event.getRawY() - initialTouchY);

                        d = Math.sqrt((initialY - params.y) * (initialY - params.y)
                                + (initialX - params.x) * (initialX - params.x));

                        if (d > 15.0) {
                            windowManager.updateViewLayout(chatHead, params);
                        }
                }

                if (retflag == 1) {
                    return false;
                } else {
                    return true;
                }

            }
        });


        chatHead.setOnClickListener(new OnClickListener() {


            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Intent intent = new Intent(HUD.this,
                        AltRadialMenuActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                //intent.addFlags();
                Intent intent1 = new Intent();
                intent1.setClass(getBaseContext(), HUD.class);
                getBaseContext().stopService(intent1);

                windowManager.updateViewLayout(chatHead, params);

            }

        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (chatHead != null)
            windowManager.removeView(chatHead);

        editor.putInt(xLocation, params.x);
        editor.putInt(yLocation, params.y);
        editor.commit();

    }


}
