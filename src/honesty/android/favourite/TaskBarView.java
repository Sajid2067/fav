package honesty.android.favourite;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.widget.TabWidget;

import honesty.android.adapter.TripHistoryTabsPagerAdapter;

import activity.BaseActivity;

public class TaskBarView extends BaseActivity {
    private String[] tabs;
    private ViewPager viewPager;
    private TabWidget tabWidget;
    private TabLayout tabLayout;
    private Toolbar toolbar;
    public static boolean isActive = false;
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       setContentView(R.layout.trip_details);

        tabLayout = (TabLayout) findViewById(R.id.tabs);

        ViewPager pager = (ViewPager) findViewById(R.id.pager);

        TripHistoryTabsPagerAdapter adapter = new TripHistoryTabsPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        pager.setCurrentItem(0);

        tabLayout.setBackgroundResource(R.color.tabColor);


        tabLayout.setupWithViewPager(pager);

      //  tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(getActivity(), R.color.indicator));

        Intent svc = new Intent(this, HUD.class);
        startService(svc);
      //  finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        isActive = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isActive = false;
    }

    @Override
    protected void setupActionBar() {
      //  Log.d(TAG, "setupActionBar()");
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        super.setupActionBar();
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        }

    }


}


