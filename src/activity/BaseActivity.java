package activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.appcompat.BuildConfig;
import android.util.Log;
import honesty.android.favourite.R;

/**
 * Created by sajid on 10/28/2015.
 */
public class BaseActivity extends AppCompatActivity {

    private static final String TAG = BaseActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate(Bundle savedInstanceState)");
        super.onCreate(savedInstanceState);
        setContentView();
        setupActionBar();
    }

    protected void setContentView() {

    }


    protected void setupActionBar() {
        Log.d(TAG, "setupActionBar()");
        if (getSupportActionBar() != null) {
            if (BuildConfig.DEBUG){
                getSupportActionBar().setLogo(R.drawable.alert_dialouge_china);
            }else {
                getSupportActionBar().setLogo(R.drawable.alert_dialouge_china);
            }

            getSupportActionBar().setDisplayUseLogoEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
          //  getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);


        }
    }

}
